#ifndef LTEXTURE_H
#define LTEXTURE_H


#include "LOpenGL.h"
#include "LFRect.h"
#include <stdio.h>
#include <string>


class LTexture
{

 public:
  // constructor
  LTexture();

  // destructor
  ~LTexture();

  bool loadTextureFromFile( std::string path );

  bool loadTextureFromPixels32( GLuint* pixels,
				GLuint width,
				GLuint height );

  void  freeTexture();

  void render( GLfloat x,
	       GLfloat y,
	       LFRect* clip = NULL );

  GLuint getTextureID();

  GLuint textureWidth();

  GLuint textureHeight();

 private:
  // texture name
  GLuint mTextureID;

  // texture dimensions
  GLuint mTextureWidth;
  GLuint mTextureHeight;
};

#endif
