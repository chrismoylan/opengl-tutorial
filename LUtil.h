#include "LOpenGL.h"

// screen constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;

// color modes
const int COLOR_MODE_CYAN = 0;
const int COLOR_MODE_MULTI = 1;

// viewport mode
enum ViewPortMode {
  VIEWPORT_MODE_FULL,
  VIEWPORT_MODE_HALF_CENTER,
  VIEWPORT_MODE_HALF_TOP,
  VIEWPORT_MODE_QUAD,
  VIEWPORT_MODE_RADAR
};


bool initGL();

bool loadMedia();

void update();

void render();

void runMainLoop( int val );

void handleKeys( unsigned char key, int x, int y );
