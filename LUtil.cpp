#include "LUtil.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include "LTexture.h"


// current color rendering mode
int gColorMode = COLOR_MODE_CYAN;

// projection scale
GLfloat gProjectionScale = 1.f;

// viewport mode
int gViewportMode = VIEWPORT_MODE_FULL;

// camera position
GLfloat gCameraX = 0.f, gCameraY = 0.f;

// checkerboard texture
LTexture gCheckerBoardTexture;

// file loaded texture
LTexture gLoadedTexture;

// sprite texture
LTexture gArrowTexture;

// sprite area
LFRect gArrowClips[ 4 ];

bool
initGL()
{
  // set the viewport
  glViewport( 0.f, 0.f, SCREEN_WIDTH, SCREEN_HEIGHT );

  // initialize projection matrix
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  glOrtho( 0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0 );

  // initialize modelview matrix
  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity();

  // save the default modelview matrix
  //glPushMatrix();

  // initialize clear color
  glClearColor( 0.f, 0.f, 0.f, 1.f );

  // enable texturing
  glEnable( GL_TEXTURE_2D );

  // check for error
  GLenum error = glGetError();
  if ( error != GL_NO_ERROR ) {
    // TODO: use propper debug macro
    printf( "Error initializing OpenGL %s\n", gluErrorString( error ) );
    return false;
  }

  // initialize DevIL
  ilInit();
  ilClearColour( 255, 255, 255, 000 );

  // check for error
  ILenum ilError = ilGetError();
  if ( ilError != IL_NO_ERROR ) {
    // TODO: debug
    printf( "Error initializing DevIL! %s\n",
      iluErrorString( ilError ) );
    return false;
  }

  return true;
}


bool
loadMedia()
{
  // set clip erctangles
  gArrowClips[ 0 ].x = 0.f;
  gArrowClips[ 0 ].y = 0.f;
  gArrowClips[ 0 ].w = 128.f;
  gArrowClips[ 0 ].h = 128.f;

  gArrowClips[ 1 ].x = 128.f;
  gArrowClips[ 1 ].y = 0.f;
  gArrowClips[ 1 ].w = 128.f;
  gArrowClips[ 1 ].h = 128.f;

  gArrowClips[ 2 ].x = 0.f;
  gArrowClips[ 2 ].y = 128.f;
  gArrowClips[ 2 ].w = 128.f;
  gArrowClips[ 2 ].h = 128.f;

  gArrowClips[ 3 ].x = 128.f;
  gArrowClips[ 3 ].y = 128.f;
  gArrowClips[ 3 ].w = 128.f;
  gArrowClips[ 3 ].h = 128.f;

  // load texture
  if ( !gArrowTexture.loadTextureFromFile( "images/arrows.png" ) ) {
    // TODO: better debug statements
    printf( "Unable to load file texture!\n" );
    return false;
  }

  return true;
}


void
update()
{
  // no real logic to update
}


void
render()
{
  // clear color buffer
  glClear( GL_COLOR_BUFFER_BIT );

  // render arrows
  gArrowTexture.render( 0.f, 0.f, &gArrowClips[ 0 ] );
  gArrowTexture.render( SCREEN_WIDTH - gArrowClips[ 1 ].w,
      0.f, &gArrowClips[ 1 ] );
  gArrowTexture.render( 0.f, SCREEN_HEIGHT - gArrowClips[ 2 ].h,
      &gArrowClips[ 2 ] );
  gArrowTexture.render( SCREEN_WIDTH - gArrowClips[ 3 ].w,
      SCREEN_HEIGHT - gArrowClips[ 3 ].h,
      &gArrowClips[ 3 ] );

//  // calculate centered offsets
//  GLfloat x = ( SCREEN_WIDTH - gLoadedTexture.textureWidth() ) / 2.f;
//  GLfloat y = ( SCREEN_HEIGHT - gLoadedTexture.textureHeight() ) / 2.f;
//
//  // render checkerboard texture
//  gLoadedTexture.render( x, y );
//
  // reset modelview matrix
//  glMatrixMode( GL_MODELVIEW );
//  glPopMatrix();
//
//  // save the default matrix again
//  glPushMatrix();
//
//  // move to center of the screen
//  glTranslatef( SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f, 0.f );
//
//  //Red quad
//  glBegin( GL_QUADS );
//    glColor3f( 1.f, 0.f, 0.f );
//    glVertex2f( -SCREEN_WIDTH / 2.f, -SCREEN_HEIGHT / 2.f );
//    glVertex2f( SCREEN_WIDTH / 2.f, -SCREEN_HEIGHT / 2.f );
//    glVertex2f( SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f );
//    glVertex2f( -SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f );
//  glEnd();
//
//  // move to the right of the screen
//  glTranslatef( SCREEN_WIDTH, 0.f, 0.f );
//
//  //Green quad
//  glBegin( GL_QUADS );
//    glColor3f( 0.f, 1.f, 0.f );
//    glVertex2f( -SCREEN_WIDTH / 2.f, -SCREEN_HEIGHT / 2.f );
//    glVertex2f( SCREEN_WIDTH / 2.f, -SCREEN_HEIGHT / 2.f );
//    glVertex2f( SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f );
//    glVertex2f( -SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f );
//  glEnd();
//
//  // move to the lower right of the screen
//  glTranslatef( 0.f, SCREEN_HEIGHT, 0.f );
//
//  //Blue quad
//  glBegin( GL_QUADS );
//    glColor3f( 0.f, 0.f, 1.f );
//    glVertex2f( -SCREEN_WIDTH / 2.f, -SCREEN_HEIGHT / 2.f );
//    glVertex2f( SCREEN_WIDTH / 2.f, -SCREEN_HEIGHT / 2.f );
//    glVertex2f( SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f );
//    glVertex2f( -SCREEN_WIDTH / 2.f, SCREEN_HEIGHT / 2.f );
//  glEnd();
//
//  // move below the screen
//  glTranslatef( -SCREEN_WIDTH, 0.f, 0.f);
//
//  // yellow quad
//  glBegin( GL_QUADS );
//    glColor3f( 1.f, 1.f, 0.f );
//    glVertex2f( -SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f );
//    glVertex2f( SCREEN_WIDTH / 4.f, -SCREEN_HEIGHT / 4.f );
//    glVertex2f( SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f );
//    glVertex2f( -SCREEN_WIDTH / 4.f, SCREEN_HEIGHT / 4.f );
//  glEnd();

  // update screen
  glutSwapBuffers();
}


void
runMainLoop( int val )
{
  // frame logic
  update();
  render();

  // run frame one more time
  glutTimerFunc( 1000 / SCREEN_FPS, runMainLoop, val );
}


void
handleKeys( unsigned char key, int x, int y )
{
  // if the user pressed w/a/s/d, change camera position
  if ( key == 'w' ) {
    gCameraY -= 16.f;
  }
  else if ( key == 's' ) {
    gCameraY += 16.f;
  }
  else if ( key == 'a' ) {
    gCameraX -= 16.f;
  }
  else  if ( key == 'd' ) {
    gCameraX += 16.f;
  }

  // take saved matrix off the stack and reset it
  glMatrixMode( GL_MODELVIEW );
  glPopMatrix();
  glLoadIdentity();

  // move camera to position
  glTranslatef( -gCameraX, -gCameraY, 0.f );

  // save default matrix again with camera translation
  glPushMatrix();
}
